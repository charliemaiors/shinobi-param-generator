module gitlab.com/charliemaiors/shinobi-param-generator

go 1.12

require (
	github.com/dave/jennifer v1.3.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.2
)
