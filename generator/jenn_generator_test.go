package generator_test

import (
	"os"
	"testing"

	"gitlab.com/charliemaiors/shinobi-param-generator/generator"
	"gitlab.com/charliemaiors/shinobi-param-generator/parser"
)

var prsr parser.ParamParser
var gen generator.ShinobiGenerator

func init() {
	prsr = parser.NewFileParser()
	gen = generator.NewSourceJenniferGenerator()
}

func TestGeneration(test *testing.T) {
	currentDir, err := os.Getwd()

	if err != nil {
		test.Fatal(err)
	}

	base := currentDir + string(os.PathSeparator) + "testpkg" + string(os.PathSeparator)
	params, err := prsr.Parse(base+"camvendors.json", base+"cams")

	if err != nil {
		test.Fatal(err)
	}

	err = gen.Generate("testpkg", base, params)

	if err != nil {
		test.Fatal(err)
	}

}
