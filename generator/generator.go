package generator

import (
	"gitlab.com/charliemaiors/shinobi-param-generator/parser"
)

//ShinobiGenerator defines an interface for save generated go file
type ShinobiGenerator interface {
	Generate(pkg, dest string, params []parser.ShinobiParam) error
}
