package generator

import (
	"fmt"

	"github.com/dave/jennifer/jen"
	"gitlab.com/charliemaiors/shinobi-param-generator/parser"
)

type sourceJenniferGenerator struct {
}

//NewJenniferGenerator define a new generator based on jennifer
func NewSourceJenniferGenerator() ShinobiGenerator {
	return &sourceJenniferGenerator{}
}

func (generator *sourceJenniferGenerator) Generate(pkg, dest string, params []parser.ShinobiParam) error {
	fmt.Printf("Dest is %s", dest)

	file := jen.NewFilePathName(dest, pkg)
	file.Comment("This is a generated file for mapping shinobi params from param source, DO NOT EDIT!!!")

	file.Comment("Path represent a path with given default values if present")
	file.Type().Id("Path").Struct(
		jen.Id("IsSecure").Bool(),
		jen.Id("SubPath").String(),
	)

	file.Comment("//Codec represent a subset of paths for a given codec")
	file.Type().Id("Codec").Struct(
		jen.Id("CodecType").String(),
		jen.Id("Paths").Index().Id("Path"),
	)

	file.Comment("//Protocol is the structure which represent a particular IPCam with it's vendor, protocol, codec and relative subpath")
	file.Type().Id("Protocol").Struct(
		jen.Id("Connection").String(),
		jen.Id("Models").Index().String(),
		jen.Id("Codecs").Id("[]Codec"),
	)

	file.Var().Id("paramsMap").Op("=").Map(jen.String()).Id("[]Protocol").Values(generator.generateDict(params))

	generator.generatePublicFunctions(dest, file)
	generator.generatePrivateFunctions(file)

	err := file.Save(dest + "types.go")
	return err
}

func (generator *sourceJenniferGenerator) generatePublicFunctions(pkg string, file *jen.File) {
	file.Comment("//TakeAllPathsForVendorWithProtocol takes vendor and protocol and select the possibles values from paramsMap")
	file.Func().Id("TakeAllPathsForVendorWithProtocol").Params(jen.Id("vendor").String(), jen.Id("protocol").String()).Parens(jen.Id("Protocol").Op(",").Error()).Block(
		jen.Id("protocolsForVendor").Op(":=").Id("paramsMap").Id("[vendor]"),
		jen.For(jen.Id("_").Op(",").Id("current").Op(":=").Range().Id("protocolsForVendor")).Block(
			jen.If(jen.Id("current").Op(".").Id("Connection").Op("==").Id("protocol")).Block(
				jen.Return(jen.Id("current"), jen.Nil()),
			),
		),
		jen.Return(jen.Id("Protocol{}"), jen.Qual("errors", "New").Call(jen.Lit("No protocol found"))),
	)

	file.Comment("//TakeProtocolForModel takes vendor and model and select the correspondent protocol")
	file.Func().Id("TakeProtocolForModel").Params(jen.Id("model").String(), jen.Id("vendor").String()).Parens(jen.Id("Protocol").Op(",").Error()).Block(
		jen.Id("protocolsForVendor").Op(":=").Id("paramsMap").Id("[vendor]"),
		jen.For(jen.Id("_").Op(",").Id("protocol").Op(":=").Range().Id("protocolsForVendor")).Block(
			jen.If(jen.Qual(pkg, "containsModel").Call(jen.Id("protocol").Op(".").Id("Models"), jen.Id("model"))).Block(
				jen.Return(jen.Id("protocol"), jen.Nil()),
			),
		),
		jen.Return(jen.Id("Protocol{}"), jen.Qual("errors", "New").Call(jen.Lit("No protocol found"))),
	)
}

func (generator *sourceJenniferGenerator) generatePrivateFunctions(file *jen.File) {
	file.Func().Id("containsModel").Params(jen.Id("models").Id("[]string"), jen.Id("model").String()).Bool().Block(
		jen.For(jen.Id("_").Op(",").Id("mod").Op(":=").Range().Id("models").Block(
			jen.If(jen.Id("mod").Op("==").Id("model").Block(
				jen.Return(jen.True()),
			)),
		)),
		jen.Return(jen.False()),
	)
}

func (generator *sourceJenniferGenerator) generateDict(params []parser.ShinobiParam) jen.Dict {
	slicePar := jen.Dict{}

	for _, param := range params {
		slicePar[jen.Lit(param.Vendor)] = jen.Id("[]Protocol").Values(generator.generateProtocols(param.Protocols)...)
	}

	return slicePar
}

func (generator *sourceJenniferGenerator) generateProtocols(prots []parser.Protocol) []jen.Code {
	protocols := make([]jen.Code, 0, 0)
	for _, prt := range prots {
		protocols = append(protocols, generator.generateProtocol(prt))
	}
	return protocols
}

func (generator *sourceJenniferGenerator) generateProtocol(prot parser.Protocol) jen.Code {
	protocol := jen.Id("Protocol").Values(jen.Dict{
		jen.Id("Connection"): jen.Lit(prot.Protocol.String()),
		jen.Id("Models"): jen.Index().String().ValuesFunc(func(g *jen.Group) {
			for _, model := range prot.Models {
				g.Lit(model)
			}
		}),
		jen.Id("Codecs"): jen.Index().Id("Codec").Values(generator.generateCodecs(prot.Codecs)...),
	})
	return protocol
}

func (generator *sourceJenniferGenerator) generateCodecs(cdcs []parser.Codec) []jen.Code {
	codecs := make([]jen.Code, 0, 0)
	for _, cdc := range cdcs {
		codecs = append(codecs, generator.generateCodec(cdc))
	}
	return codecs
}

func (generator *sourceJenniferGenerator) generateCodec(cdc parser.Codec) jen.Code {
	codecPar := jen.Id("Codec").Values(jen.Dict{
		jen.Id("CodecType"): jen.Lit(cdc.Type.String()),
		jen.Id("Paths"): jen.Index().Id("Path").ValuesFunc(func(g *jen.Group) {
			for _, path := range cdc.Paths {
				g.Id("Path").Values(jen.Dict{
					jen.Id("IsSecure"): jen.Lit(path.IsSecure),
					jen.Id("SubPath"):  jen.Lit(path.SubPath),
				})
			}
		}),
	})

	return codecPar
}
