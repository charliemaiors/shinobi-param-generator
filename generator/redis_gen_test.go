package generator_test

import (
	"os"
	"testing"

	"gitlab.com/charliemaiors/shinobi-param-generator/generator"
)

func TestRedisGeneration(test *testing.T) {
	redisgen := generator.NewRedisGenerator(os.Getenv("REDIS_HOST")+":6379", "", 0)
	currentDir, err := os.Getwd()

	if err != nil {
		test.Fatal(err)
	}

	base := currentDir + string(os.PathSeparator) + "testpkg" + string(os.PathSeparator)
	params, err := prsr.Parse(base+"camvendors.json", base+"cams")

	if err != nil {
		test.Fatal(err)
	}

	err = redisgen.Generate("testpkg", base, params)

	if err != nil {
		test.Fatal(err)
	}

}
