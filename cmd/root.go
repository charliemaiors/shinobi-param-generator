// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/charliemaiors/shinobi-param-generator/generator"
	"gitlab.com/charliemaiors/shinobi-param-generator/parser"
)

var paramFile, paramFolder, pkg, genDest, redisAddr, redisPasswd string
var redisDB int
var prsr parser.ParamParser
var gnr generator.ShinobiGenerator
var redisGen bool

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "shinobi-params",
	Short: "Shinobi params generate params for shinobi vendors",
	Long:  `Shinobi params generate params for shinobi vendors from params file (for now)`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: generate,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(viper.AutomaticEnv)

	prsr = parser.NewFileParser()

	curDir, err := os.Getwd()

	if err != nil {
		fmt.Printf("Error evaluating current directory %v, if flag --dest is not specified you can get a panic", err)
	}
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	RootCmd.PersistentFlags().StringVar(&paramFile, "params-file", "camvendors.json", "params file (default is in the local directory named camvendors.json)")
	RootCmd.PersistentFlags().StringVar(&paramFolder, "params-folder", "cams", "params folder (default is the  directory named cams, used for test purposes)")
	RootCmd.PersistentFlags().StringVar(&genDest, "dest", curDir+string(os.PathSeparator), "generated file destination (default current directory, if is writtable)")
	RootCmd.PersistentFlags().BoolVar(&redisGen, "redis", false, "use generator for redis (also populate redis with data)")
	RootCmd.PersistentFlags().StringVar(&redisAddr, "redis-addr", "", "redis address (host:port) if redis flag is provided")
	RootCmd.PersistentFlags().StringVar(&redisPasswd, "redis-passwd", "", "redis password (if settled) when redis flag is provided")
	RootCmd.PersistentFlags().IntVar(&redisDB, "redis-db", -1, "redis db index")
	RootCmd.PersistentFlags().StringVar(&pkg, "target-package", "main", "destination package default is main")
}

func generate(*cobra.Command, []string) {
	if redisGen && redisAddr == "" && redisDB == -1 {
		fmt.Print("Redis is enabled without any parameter")
		os.Exit(1)
	} else if redisGen {
		gnr = generator.NewRedisGenerator(redisAddr, redisPasswd, redisDB)
	} else {
		gnr = generator.NewSourceJenniferGenerator()
	}
	params, err := prsr.Parse(paramFile, paramFolder)
	if err != nil {
		panic(err)
	}

	err = gnr.Generate(pkg, genDest, params)
	if err != nil {
		panic(err)
	}
}
