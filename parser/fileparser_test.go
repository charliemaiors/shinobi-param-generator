package parser_test

import (
	"os"
	"testing"

	"gitlab.com/charliemaiors/shinobi-param-generator/parser"
)

var parserInstance parser.ParamParser

func init() {
	parserInstance = parser.NewFileParser()
}

func TestParse(t *testing.T) {
	sourceDir, err := os.Getwd()

	if err != nil {
		t.Fatal(err)
	}

	vendorBrands := sourceDir + string(os.PathSeparator) + "camvendors.json"
	vendorParams := sourceDir + string(os.PathSeparator) + "cams"

	_, err = parserInstance.Parse(vendorBrands, vendorParams)

	if err != nil {
		t.Fatal(err)
	}

	t.Log("Success!!!")
}
