// generated by jsonenums -type=CodecType; DO NOT EDIT

package parser

import (
	"encoding/json"
	"fmt"
)

var (
	_CodecTypeNameToValue = map[string]CodecType{
		"JPEG":     JPEG,
		"MJPEG":    MJPEG,
		"H264":     H264,
		"HLS":      HLS,
		"MPEG4":    MPEG4,
		"Streamer": Streamer,
		"Dashcam":  Dashcam,
	}

	_CodecTypeValueToName = map[CodecType]string{
		JPEG:     "JPEG",
		MJPEG:    "MJPEG",
		H264:     "H264",
		HLS:      "HLS",
		MPEG4:    "MPEG4",
		Streamer: "Streamer",
		Dashcam:  "Dashcam",
	}
)

func init() {
	var v CodecType
	if _, ok := interface{}(v).(fmt.Stringer); ok {
		_CodecTypeNameToValue = map[string]CodecType{
			interface{}(JPEG).(fmt.Stringer).String():     JPEG,
			interface{}(MJPEG).(fmt.Stringer).String():    MJPEG,
			interface{}(H264).(fmt.Stringer).String():     H264,
			interface{}(HLS).(fmt.Stringer).String():      HLS,
			interface{}(MPEG4).(fmt.Stringer).String():    MPEG4,
			interface{}(Streamer).(fmt.Stringer).String(): Streamer,
			interface{}(Dashcam).(fmt.Stringer).String():  Dashcam,
		}
	}
}

// MarshalJSON is generated so CodecType satisfies json.Marshaler.
func (r CodecType) MarshalJSON() ([]byte, error) {
	if s, ok := interface{}(r).(fmt.Stringer); ok {
		return json.Marshal(s.String())
	}
	s, ok := _CodecTypeValueToName[r]
	if !ok {
		return nil, fmt.Errorf("invalid CodecType: %d", r)
	}
	return json.Marshal(s)
}

// UnmarshalJSON is generated so CodecType satisfies json.Unmarshaler.
func (r *CodecType) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("CodecType should be a string, got %s", data)
	}
	v, ok := _CodecTypeNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid CodecType %q", s)
	}
	*r = v
	return nil
}
