package parser

import (
	"encoding/json"
	"io/ioutil"
	"net/url"
	"os"
)

type fileParser struct{}

//NewFileParser returns a new instance of fileParser
func NewFileParser() ParamParser {
	return &fileParser{}
}

func (parser *fileParser) Parse(camBrands, camParamFolder string) ([]ShinobiParam, error) {
	bytes, err := ioutil.ReadFile(camBrands)

	if err != nil {
		return nil, err
	}

	vendors := make([]string, 0, 0)
	err = json.Unmarshal(bytes, &vendors)
	if err != nil {
		return nil, err
	}

	params := make([]ShinobiParam, 0, 0)
	for _, vendor := range vendors {
		tmp, err := parser.parseVendor(camParamFolder, vendor)
		if err != nil {
			return nil, err
		}

		params = append(params, tmp)
	}
	return params, nil
}

func (parser *fileParser) parseVendor(vendorFolder, vendor string) (ShinobiParam, error) {
	effectiveVendor, err := url.PathUnescape(vendor)

	if err != nil {
		return ShinobiParam{}, err
	}
	bytes, err := ioutil.ReadFile(vendorFolder + string(os.PathSeparator) + effectiveVendor + ".json")
	if err != nil {
		return ShinobiParam{}, err
	}
	protocols := make([]Protocol, 0, 0)
	err = json.Unmarshal(bytes, &protocols)
	res := ShinobiParam{}

	if err != nil {
		return res, err
	}

	return ShinobiParam{
		Vendor:    vendor,
		Protocols: protocols,
	}, nil
}
