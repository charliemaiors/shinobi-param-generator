package parser

import (
	"encoding/json"
	"fmt"
	"strings"
)

const (
	usernamePattern = "[USERNAME]"
	passwordPattern = "[PASSWORD]"
	widthPattern    = "[WIDTH]"
	widthValue      = "1280"
	heightPattern   = "[HEIGHT]"
	heightValue     = "720"
	channelPattern  = "[CHANNEL]"
	channelValue    = "0"
)

type ShinobiParam struct {
	Vendor    string
	Protocols []Protocol
}

type Protocol struct {
	Protocol Connection `json:"protocol"`
	Models   []string   `json:"models"`
	Codecs   []Codec    `json:"codec"`
}

type Codec struct {
	Type  CodecType `json:"type"`
	Paths []Path    `json:"paths"`
}

type Path struct {
	SubPath  string `json:"subpath"`
	IsSecure bool   `json:"isSecure"`
}

type CodecType int

const (
	JPEG CodecType = iota
	MJPEG
	H264
	HLS
	MPEG4
	Streamer
	Dashcam
)

type Connection int

const (
	HTTP Connection = iota
	HTTPS
	RTSP
	RTMP
	RTMPS
	UDP
)

var valueToConnection = map[string]Connection{
	"http://":  HTTP,
	"https://": HTTPS,
	"rtsp://":  RTSP,
	"rtmp://":  RTMP,
	"rtmps://": RTMPS,
	"udp://":   UDP,
}

var connectionToValue = map[Connection]string{
	HTTP:  "http://",
	HTTPS: "https://",
	RTSP:  "rtsp://",
	RTMP:  "rtmp://",
	RTMPS: "rtmps://",
	UDP:   "udp://",
}

type ParamParser interface {
	Parse(camBrands, camParamFolder string) ([]ShinobiParam, error)
}

func (codec CodecType) String() string {
	names := []string{
		"JPEG", "MJPEG", "H.264", "HLS", "MPEG-4", "Shinobi Streamer", "Dashcam",
	}

	return names[codec]
}

func (connection Connection) String() string {
	return connectionToValue[connection]
}

//MarshalJSON implementation of marshaler interface
func (connection Connection) MarshalJSON() ([]byte, error) {
	return json.Marshal(connection.String())
}

//UnmarshalJSON implementation of marshaler interface
func (connection *Connection) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	v, ok := valueToConnection[s]
	if !ok {
		return fmt.Errorf("No valid connection found")
	}
	*connection = v
	return nil
}

func (path Path) String() string {
	return fmt.Sprintf(`{"subpath":"%s, "isSecure": "%v"`, path.SubPath, path.IsSecure)
}

func (path Path) MarshalJSON() ([]byte, error) {
	return json.Marshal(path.String())
}

func (path *Path) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}

	if strings.Contains(s, usernamePattern) && strings.Contains(s, passwordPattern) {
		path.IsSecure = true
	}
	replacer := strings.NewReplacer(usernamePattern, "%s", passwordPattern, "%s", widthPattern, widthValue, heightPattern, heightValue, channelPattern, channelValue)
	validPath := replacer.Replace(s)
	path.SubPath = validPath
	return nil
}
